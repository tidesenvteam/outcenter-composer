FROM fedora:latest

RUN dnf -y install \
    git \
    composer \
    php \
    php-common \
    php-cli \
    php-mbstring \
    php-xml \
    php-mysqlnd \
    php-pdo \
    php-gd \
    php-intl \
    php-interbase \
    php-zip \
    php-json \
    php-pecl-mcrypt \
    php-pecl-ssh2 \
    openssl \
    openssh \
    php-snmp

ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /sbin/tini
RUN chmod +x /sbin/tini

RUN mkdir /root/.ssh/
RUN echo -e "-----BEGIN OPENSSH PRIVATE KEY-----\nb3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAACFwAAAAdzc2gtcn\nNhAAAAAwEAAQAAAgEAqHs9Rs9oodnRnrvrLc9zZxc9HTRrjM/Vpv618Jo9sSdVLmto9gHd\nQN42/EyucwAbglHCwgE6AUTi45DR/32YfOETqLbnR9hMV/vLMqMDncZEliQhtBFWRtUhDL\ngm+/teInxrbsfed37aZZJbHHptcWa2Nl86E4C/9WHJ2pY24WOhs+VoNGcETo+Ue5qhUJJY\nqOM3ssYx10RNq8N/cjSQy384V47J8D6zdPr0brUchjSLFms8YNpu1doNeHt4n8jqty1S8T\nAk+pQ5vt1m9SJMh2l32r/zO4ymQuC0Yis9LoZo6bU/PiHfsD5YTl/gRY155w+iZ2OGRVUN\npVsHq89yTtEbayRpBAbElbxnBBb+pmRjzmgKswCLlgVEidbiqoU7wTgqyQPsHaYtUqJr8Y\n1iZ6Nxb6nA2IRNvOQTFH+dl8sido85gN3z0QNi6A7YDXVmbRnadippM/lJqOU7jcQiG0Qw\nezIRdASGBhr7dNIPX3F8n38GEpCu71lgotbpA9sj6G+t/PAX/bva1HO0PKZr5bKD7oAUfa\nmrYSVBhuPyDTjpUuG2somom1uZETJVQ1NwhnPDRT307ybdFbC9lAysHikgZLtdME2XeWbl\nnG+Mlcaf3ZsiXMVqMD+Zvox8HHsf2PZsC/gA0huWE4yoNytegr8b0zcBVJoPZCRIRvbuY0\nkAAAdYzizE+84sxPsAAAAHc3NoLXJzYQAAAgEAqHs9Rs9oodnRnrvrLc9zZxc9HTRrjM/V\npv618Jo9sSdVLmto9gHdQN42/EyucwAbglHCwgE6AUTi45DR/32YfOETqLbnR9hMV/vLMq\nMDncZEliQhtBFWRtUhDLgm+/teInxrbsfed37aZZJbHHptcWa2Nl86E4C/9WHJ2pY24WOh\ns+VoNGcETo+Ue5qhUJJYqOM3ssYx10RNq8N/cjSQy384V47J8D6zdPr0brUchjSLFms8YN\npu1doNeHt4n8jqty1S8TAk+pQ5vt1m9SJMh2l32r/zO4ymQuC0Yis9LoZo6bU/PiHfsD5Y\nTl/gRY155w+iZ2OGRVUNpVsHq89yTtEbayRpBAbElbxnBBb+pmRjzmgKswCLlgVEidbiqo\nU7wTgqyQPsHaYtUqJr8Y1iZ6Nxb6nA2IRNvOQTFH+dl8sido85gN3z0QNi6A7YDXVmbRna\ndippM/lJqOU7jcQiG0QwezIRdASGBhr7dNIPX3F8n38GEpCu71lgotbpA9sj6G+t/PAX/b\nva1HO0PKZr5bKD7oAUfamrYSVBhuPyDTjpUuG2somom1uZETJVQ1NwhnPDRT307ybdFbC9\nlAysHikgZLtdME2XeWblnG+Mlcaf3ZsiXMVqMD+Zvox8HHsf2PZsC/gA0huWE4yoNytegr\n8b0zcBVJoPZCRIRvbuY0kAAAADAQABAAACACohRx1h2ARA943957041t68jOe0qAA6KG7t\n2okjyLF36HEHIuQBa1ZBOva0JhtTt41K7KyeqeaPVz0YhaqhuXPgxI23zvXEPLLzn8g1wz\n28yoHQ3yyr+SsVOIJ5uiXgoZV2D1dQW2z+J5amlmn0o1FsIbxO67LcyyONJHCoh2itBU56\n2yffatBKTxLpg9UmA02YI+FsdiIiTlnZNRFV269HTMt4RT7yLuXdqynT2CInGXCtuBdwB6\n8qK5wpI+ywzSCpEMiHBXgzgI0dyBUcUo657OKihSwq65hloDW8Ijx0F9/5FqtuxQTJUkOm\n7en2irkgy7eWx4im0Ba9QJnR8f8joi04ktU6xpcH5oxA7t4Hjido1AfK5xZqtXgMoInCY6\nzuAFLPaADQKk9zN958/jRn5zlTF9RITF2jD8vsBA8ZBsncrYYsOdSgOolhsbNXrBYdjDhX\njW9cOLlP53U96EJwst9cQmlUTKUbuICHljEGulrnHNieDDyDIDWfH9dlDGl1bvvAoFLI4F\nvEA3vse98jm8xbg4NY/5aLNYbBWGawIpUVljjDAFLAA3MuiMJURH+mdWBXlVOtj1kOb6C1\nfXpEhJLZXu1R/u/UHxrdqfjP/FSrlrjl3lVKmSbTDO6eYxb3jjUzH1U4gGuSVHoH2M7s+q\nPbgRPhsj/+A5j8jz5RAAABAHmm6ddf8GZQal6x1Q+Srtg5lLu5SivpLraUdJsxOrNWxS/u\ncyXEoQ+9NgIyqRZs4M7taCvxL7E9f23ZApwzqrMBaSsslvmO4J0hK5lrR6udJItceG/UDa\ncS6Ke8g5XOu1TJ/CCC2hT/LW/hB7QSnXObNTQwvljIlcvN/YAJ0DvG5HQKjOOLIgr/RQad\nx28DTvAMRhNY2rvxDBimMbJ00KTb8CJcFqKp9mJlFBveq2SS0DHW3RYHLLjmLZGWgDEG9K\nYTJgV8gkH8MzreptfA4nuUfQnKDVZyQS32p3VaeaOaJuS3EJ0LY6DQRpwF3+IcQpgUL1N6\nPQQ4EyWAEEuAJ/UAAAEBANLtGCR39iSkoaSm5ZzuVCE9cbl2PEz0/+fv/LbO2q8FSx0XH7\nIckOwVIYFNAuX9LtNf6moLfsBERVihKxMKChcupqasVC+zwe1hwZTwlKRbkksjJzEv74go\n9W9FSC0R/iHKDFateIdFucD9vSHhez/Ke0+eEN1wtreDLbc3ktu67qAz6X7PdLl220Z3CN\nwd2ECQNnZP/pv18PgeE6LOrvfEZhgpv/yNkgTE36ploK/Q4QZZbkrbWEZBSixFsMh8o0/e\nYgt4IBUwT6e2PJcbApgsHPwsiuvZjGp60QQTpotTaqxThmhAzXqVGQEOtNmSRfmaN1KgOD\n0gVcAxltxhRBcAAAEBAMx8Krd1XmaoZFbF3YAzdDdWXdKUdDgCyhLlqQXzGFX4vK/1V/f5\n6TOkGex08nO1rjBM/J76x+ykVh/S521zkz9Dj164PpYhjZXcglM9j0olkfy7Fy2e4nCgZV\nhVCxVCOJzZh21TxiW93t9e8JxEQFhCNKiBcTCy9xiDJ8cxHvD6pq1Kcaz0gJ4dqwCi/a3Q\npspycEEBj8qd43q+Ube/VF+d8Fimx4rm1Zi3Ro6vczJ7aAVsO5j4i+W/XtQgLCsoTEQv9l\njjZY9gstph8LxP+2AcE3oajZt3mMIWWGDr5wtz3miB6/QHnsgH+dClACijdDrkAmVlTo6z\na104wfjOT58AAAAgZGVzZW52b2x2aW1lbnRvQG91dGNlbnRlci5jb20uYnIBAgM=\n-----END OPENSSH PRIVATE KEY-----" > /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa

RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

RUN mkdir  /.composer
RUN chmod 777 /.composer

COPY docker-entrypoint.sh /docker-entrypoint.sh

RUN chmod +x /docker-entrypoint.sh

WORKDIR /app

ENTRYPOINT ["/bin/sh", "/docker-entrypoint.sh"]

CMD ["composer"]